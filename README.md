# menulibre

[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0)

An advanced menu editor that provides modern features in a clean, easy-to-use interface

https://github.com/bluesabre/menulibre

<br><br>

Don't compile under chroot enbironment.

<br><br>
How to clone this repository:

```
git clone https://gitlab.com/azul4/content/gnome/menulibre.git
```
